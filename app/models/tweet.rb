# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user
  validates :body, length: { maximum: 180, minimum: 1 }
  validate :unique_tweet_in_last_day

  scope :no_company_affiliation, -> { joins(:user).where(users: { company_id: nil }) }

  def unique_tweet_in_last_day
    if Tweet.where(user_id: user_id, body: body).where("created_at > ?", 24.hours.ago).exists?
      errors.add(:body, "Tweet should be unique in the last 24h")
    end
  end
end
