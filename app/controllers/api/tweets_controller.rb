module Api
  class TweetsController < ApplicationController
    include Indexable
    skip_before_action :verify_authenticity_token

    def create
      @tweet = Tweet.create(
        user_id: params[:user_id],
        body: params[:body]
      )

      return render json: @tweet.errors, status: :unprocessable_entity if @tweet.errors.any?

      render json: @tweet
    end
  end
end
