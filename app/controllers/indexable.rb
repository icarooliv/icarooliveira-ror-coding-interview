module Indexable
  TWEETS_PER_PAGE = 50

  def index
    @tweets = Tweet
              .includes(user: [:company])
              .order('created_at ASC')
              .limit(TWEETS_PER_PAGE)
              .offset(offset)

    @tweets = @tweets.no_company_affiliation if page_params[:affiliated] == 'false'

    respond_to do |format|
      format.json { render json: @tweets }
      format.html { render :index }
    end
  end

  def page_params
    params.permit(:page, :affiliated).merge(per_page: TWEETS_PER_PAGE)
  end

  def page
    params[:page]
  end

  def offset
    return 0 if !page || page == 1

    page * (page.to_i - 1)
  end
end
