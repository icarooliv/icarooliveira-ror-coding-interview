require 'rails_helper'

RSpec.describe Tweet, type: :model do
  describe "validations" do
    let(:user) { create(:user)}
    let(:tweet) { create(:tweet, user: user)}

    context "body length" do
      it "should validate the length of a tweet's body" do
        valid_tweet1 = Tweet.new(user: user, body: Faker::Lorem.characters(number: 179))
        valid_tweet2 = Tweet.new(user: user, body: Faker::Lorem.characters(number: 180))
        invalid_tweet = Tweet.new(user: user, body: Faker::Lorem.characters(number: 181))

        expected_errors = ["is too long (maximum is 180 characters)"]

        expect(valid_tweet1).to be_valid
        expect(valid_tweet2).to be_valid
        expect(invalid_tweet).not_to be_valid
        expect(invalid_tweet.errors[:body]).to eq(expected_errors)
      end
    end

    context 'unique tweets in the last day' do
      it 'should validates the uniquenes of a tweet body in the past 24h' do
        tweet1 = Tweet.create(user: user, body: "My repeated tweet")
        tweet2 = Tweet.new(user: user, body: "My repeated tweet")

        expected_errors = ["Tweet should be unique in the last 24h"]

        expect(tweet2).not_to be_valid
        expect(tweet2.errors[:body]).to eq(expected_errors)
      end

      it 'should not add an error for a repeated message with over 24h' do
        tweet1 = Tweet.create(user: user, body: "My repeated tweet", created_at: 25.hours.ago)
        tweet2 = Tweet.new(user: user, body: "My repeated tweet")

        expect(tweet2).to be_valid
      end
    end
  end
end
