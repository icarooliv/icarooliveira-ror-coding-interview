#### Ruby on Rails Coding Interview

Project Requirements:

* Ruby version: 3.1.0
* Node version: 16.16.0

#### Before the coding interview:

* Clone the repository locally and install all the dependencies
* Get familiar with the codebase
* Check the database schema and make sure your database is up and running.
* Ensure that you can run the app locally
* Run `rails db:seed` to get some sample data.

#### To start the app:

Run server `rails s` or `./bin/dev` if you need to work with React part 

### Note to reader

This is the code that was solved during the interview: 
* https://gitlab.com/icarooliv/icarooliveira-ror-coding-interview/-/commit/655783f6cf62ed7ad094bf2d1f27e40835a62a1e
* https://gitlab.com/icarooliv/icarooliveira-ror-coding-interview/-/commit/569a4e3fe2df7bf14e366cf23708f88eda9a0ded

The code added before was made after the interview.